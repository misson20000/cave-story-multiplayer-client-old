#!/usr/bin/python

import os
import sys
import pdb

print("=== TSC Converter (cross-platform) ===")
print("===   Originally By Carrot Lord    ===")
print("")

pyMajorVersion = sys.version_info[0]
pyMinorVersion = sys.version_info[1]

encoding = "latin-1"
def encode(o): return o.encode(encoding)
if pyMajorVersion == 3:
	hexTuple = [encode(chr(i)) for i in range(256)]
else:
	hexTuple = [encode(unichr(i)) for i in xrange(256)]

def tsc_to_txt(cipher, src, dest):
	offsetSrc = 0
	offsetDest = 0
	while True:
		src.seek(offsetSrc, 0)
		dest.seek(offsetDest, 0)
		tscChar = src.read(1)
		if len(tscChar) != 1:
			#print("EOF")
			break
		else:
			src.seek(offsetSrc, 0)
			if ord(tscChar) == cipher: dest.write(hexTuple[cipher])
			else: dest.write(hexTuple[ord(tscChar) - cipher])
			
			#1. ord() will convert a char -> integer ASCII value
			#2. Subtract 0x20 to decode each char
			#3. hexTuple to convert to hex string
			#4. Write to file

			offsetDest += 1
			offsetSrc += 1

def txt_to_tsc(cipher, middleLocation, src, dest):
	offsetSrc = 0
	offsetDest = 0
	while True:
		dest.seek(offsetDest, 0)
		src.seek(offsetSrc, 0)
		txtChar = src.read(1)
		if len(txtChar) != 1:
			#print("EOF")
			break
		else:
			src.seek(offsetSrc, 0)
			if offsetSrc == middleLocation:
				dest.write(hexTuple[cipher])
			else:
				if ord(txtChar) + cipher > 255: dest.write(encode("\xff"))
				else: dest.write(hexTuple[ord(txtChar) + cipher])

			#1. ord() will convert a char -> integer ASCII value
			#2. Add 0x20 to encode each char
			#3. hexTuple to convert to hex string
			#4. Write to file

			offsetSrc += 1
			offsetDest += 1

def get_cipher(src):
	# Note that the code1 must always be 0D
	newlineDictionary = {}
	offset = 0
	while True:
		# Look for CRLF pairs
		src.seek(offset, 0)
		tscChar = src.read(1)
		tscChar2 = src.read(1)
		if len(tscChar) != 1 or len(tscChar2) != 1: break # Break on EOF

		code1 = ord(tscChar)
		code2 = ord(tscChar2)
		if (code1 - code2) == 3: # If the difference is 3, i.e. a newline character
			if code1 in newlineDictionary: newlineDictionary[code1] += 1
			else: newlineDictionary[code1] = 1

		offset += 1

	keys = list(newlineDictionary.keys())
	topValue = 0
	topKey = 0
	for key in keys:
		if newlineDictionary[key] > topValue:
			topKey = key
			topValue = newlineDictionary[key]

	# 0x0D compensates for ASCII 0D
	# The topkey is the cipher, in decimal.
	return topKey - 0x0D 

def make_cipher(srcName, src):
	middleLocation = int(round(os.path.getsize(srcName) / 2))
	src.seek(middleLocation, 0)
	return (ord(src.read(1)), middleLocation) # This is the [cipher in decimal, endfilelocation]

def convertFile(filename):
	checkExt = filename[-4:]
	if not checkExt in ['.txt', '.tsc']: raise ValueError
		
	filename = filename[:-4]
	tsc = (checkExt == '.tsc')
	dest = filename + ('.txt' if tsc else '.tsc')
	src = filename + checkExt
		
	with open(src, "rb") as srcObj, open(dest, "wb") as destObj:
		print("Converting file " + src + "...")
		if tsc:
			cipher = get_cipher(srcObj)
			tsc_to_txt(cipher, srcObj, destObj)
		else:
			cipher = make_cipher(src, srcObj)
			txt_to_tsc(cipher[0], cipher[1], srcObj, destObj)

		print("Conversion successful! Saved as " + dest)

def main():
	if pyMajorVersion == 2 and pyMinorVersion < 7:
		print("This script is not guaranteed to work on a version of Python below 2.7")

	exit = False
	while not exit:
		if pyMajorVersion == 3:
			target = input("Please enter filename or directory: ")
		else:
			target = raw_input("Please enter filename or directory: ")

		if os.path.isdir(os.path.join(os.getcwd(), target)):
			for path, dirs, files in os.walk(os.path.join(os.getcwd(), target)):
				for file in files:
					try: convertFile(os.path.join(os.getcwd(), target, file))
					except Exception as inst: pass #print('{}'.format(inst))

		else:
			try: convertFile(target)
			except ValueError:
				print("---------------")
				print("")
				print("Error. Please enter a valid filename that has an extension of either .txt or .tsc")
				if pyMajorVersion == 3:
					input("Exiting program...")
				else:
					raw_input("Exiting program...")
				exit = True
				continue

		if pyMajorVersion == 3:
			choice = input("Exit? ")
		else:
			choice = raw_input("Exit ?")

		try:
			if choice in ['yes', 'y', 'quit', 'exit']: exit = True
		except ValueError: pass

	sys.exit()

main()