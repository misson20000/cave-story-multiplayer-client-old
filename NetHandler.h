#ifndef NETHANDLER_H
#define NETHANDLER_H
#include"common/NetStream.h"
class NetHandler {
public:
  NetHandler(NetStream *str);
  ~NetHandler();
  void run();
  void handlePackets();
  void update_pos(int x, int y);
  void update_frame(int frame);
  void update_dir(uint8_t dir);
  void activate();
  void nod();
  void changeTBoxBusy(bool busy);
  void textbox_ynj_result(bool result);
  void fire_pressed();
  void fire_released();
  void change_weapon(int wpn);
  void update_look(char look);

  void disconnect();

  bool ltbox_busy;
private:
  NetStream *stream;
  int lx;
  int ly;
  int lf;
  uint8_t ldir;
  char llook;
};
#endif
