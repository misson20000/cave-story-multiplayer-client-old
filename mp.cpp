#include<thread>
#include "nx.h"
#include "common/NetStream.h"
#include "NetHandler.h"

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<netdb.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<sys/socket.h>

#include<arpa/inet.h>


bool MP::init() {  
  initted = true;
  return false;
}
bool MP::connectSocket() {
  if(!initted) {
    if(init())
    { return true; }
  }
  int sock, rv;
  struct addrinfo hints, *servinfo, *p;
  char s[INET6_ADDRSTRLEN];

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  if((rv = getaddrinfo(addr, port, &hints, &servinfo)) != 0) {
    return 1;
  }

  for(p = servinfo; p != NULL; p = p->ai_next) {
    if((sock = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      continue;
    }

    if(connect(sock, p->ai_addr, p->ai_addrlen) == -1) {
      close(sock);
      continue;
    }

    break;
  }
  if(p == NULL) {
    return 1;
  }
  freeaddrinfo(servinfo);
  
  NetStream *stream = new NetStream(sock);
  stream->writeBytes("HELLO", 5);
  hand = new NetHandler(stream);
  return false;
}
