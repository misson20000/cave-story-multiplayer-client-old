#include "nx.h"
#include<list>

struct Chat {
  int age;
  char *str;
};

std::list<Chat> chatlog;

void DrawChat() {
  int j = 1;
  for(std::list<Chat>::iterator i = chatlog.begin(); i != chatlog.end();) {
    font_draw_shaded(8, (SCREEN_HEIGHT - 4) - (j * (GetFontHeight() + 1)), (*i).str, 0, &greenfont);
    if((*i).age > 200) {
      i = chatlog.erase(i);
    } else {
      (*i).age++;
      i++;
    }
    j++;
  }
}

void ChatAdd(char *msg) {
  chatlog.push_back({0, msg});
  printf("CHAT: %s", msg);
}
