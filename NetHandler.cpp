#include"NetHandler.h"
#include"nx.h"
#include"TextBox/YesNoPrompt.h"
#include<chrono>
#include<atomic>
#include"playerstats.fdh"
#include"p_arms.fdh"
#define TUR_PARAMS		(TB_LINE_AT_ONCE | TB_VARIABLE_WIDTH_CHARS | TB_CURSOR_NEVER_SHOWN)

typedef bool (*PacketHandler)(NetStream *stream);
Caret *effect(int x, int y, int effectno);
void music(int songno);
int music_lastsong();
Object *FindObjectByID2(int id2);
void map_focus(Object *o, int spd);

#define LOOKUP(ACTION) {			\
    if(UIDLookup[uid]) {			\
      UIDLookup[uid]->ACTION;			\
    }						\
  }

#define LOOKUP_STORE(VAR) {			\
    if(UIDLookup[uid]) {			\
      VAR = UIDLookup[uid];			\
    }						\
  }


bool handle_handshake(NetStream *stream) {
  stat("= Beginning new game =");
  
  memset(game.flags, 0, sizeof(game.flags));
  memset(game.skipflags, 0, sizeof(game.skipflags));
  textbox.StageSelect.ClearSlots();
  
  game.quaketime = game.megaquaketime = 0;
  game.showmapnametime = 0;
  game.debug.god = 0;
  game.running = true;
  game.frozen = false;
  
  // fully re-init the player object
  Objects::DestroyAll(true);
  game.createplayer();
  
  player->maxHealth = 3;
  player->hp = player->maxHealth;
  player->iscurly = stream->readByte();
  return false;
}

bool handle_change_map(NetStream *stream) {
  int no,x,y;
  game.switchstage.mapno = no = stream->readByte();
  game.switchstage.playerx = x = stream->readInt();
  game.switchstage.playery = y = stream->readInt();
  game.switchstage.eventonentry = 0;
  return true; // Wait for next tick before processing more packets
}

bool handle_spawn(NetStream *stream) {
  game.setmode(GM_NORMAL);
  player->hide = false;
  player->hp = stream->readByte();
  player->maxHealth = stream->readByte();
  textbox.ClearText();
  return true;
}

bool handle_clear_objects(NetStream *stream) {
  Objects::DestroyAll(false);
  FloatText::ResetAll();
  return false;
}

bool handle_new_object(NetStream *stream) {
  int x = stream->readInt();
  int y = stream->readInt();
  int id1 = stream->readShort();
  int id2 = stream->readShort();
  int type = stream->readShort();
  int flags = stream->readShort();
  int uid = stream->readShort();
  
  int dir = (flags & FLAG_FACES_RIGHT) ? RIGHT : LEFT;
  Object *o = CreateObject(x, y, type, 0, 0, dir, NULL, CF_NO_SPAWN_EVENT);
  o->id1 = id1;
  o->id2 = id2;
  o->flags |= flags;
  o->uid = uid;
  
  UIDLookup[uid] = o;
  ID2Lookup[id2] = o;
  
  o->OnSpawn();
  return false;
}

bool handle_kill_object(NetStream *stream) {
  int uid = stream->readShort();
  LOOKUP(Destroy());
  return false;
}

bool handle_object_pos(NetStream *stream) {
  int uid = stream->readShort();
  int x = stream->readInt();
  int y = stream->readInt();
  LOOKUP(x = x);
  LOOKUP(y = y);
  return false;
}

bool handle_object_frame(NetStream *stream) {
  int uid = stream->readShort();
  int frame = stream->readInt();
  
  LOOKUP(frame = frame);
  return false;
}

bool handle_object_dir(NetStream *stream) {
  int uid = stream->readShort();
  char dir = stream->readByte();
  LOOKUP(dir = dir);
  return false;
}

bool handle_object_type(NetStream *stream) {
  int uid = stream->readShort();
  short type = stream->readShort();
  LOOKUP(type = type);
  LOOKUP(sprite = objprop[type].sprite);
  return false;
}

bool handle_caret(NetStream *stream) {
  int x = stream->readInt();
  int y = stream->readInt();
  int no = stream->readShort();
  
  effect(x, y, no);
  return false;
}

bool handle_tsc(NetStream *stream) {
  char op = stream->readByte();
  return false;
}

bool v_nod;
bool v_nod_lastfire;
bool v_nod_lastjump;

bool handle_tbox(NetStream *stream) {
  char op = stream->readByte();
  
  switch(op) {
  case 0:
    textbox.ClearText();
    break;
  
  case 1:
    textbox.YesNoPrompt.SetVisible(false);
    break;
  
  case 2: 
    textbox.SetFlags(TUR_PARAMS, false);
    textbox.SetVisible(true, TB_DEFAULTS);
    break;
  
  case 3: {
    int len = stream->readInt();
    char *buf = (char *) calloc(sizeof(char), len);
    stream->readBytes(buf, len);
    textbox.AddText(buf);
    break;
  }
  
  case 4:
    v_nod = true;
    v_nod_lastjump = true;
    v_nod_lastfire = true;
    textbox.ShowCursor(true);
    break;
  
  case 5:
    textbox.SetVisible(false);
    textbox.ClearText();
    break;
  
  case 6:
    textbox.YesNoPrompt.SetVisible(true);
    break;

  case 7: //<TUR
    textbox.SetFlags(TUR_PARAMS, true);
    textbox.SetCanSpeedUp(false);
    break;

  case 8: {
    int no = stream->readShort();
    if(no != 0) {
      int sprite, frame;
      
      if(no >= 1000) {
	// an item
	sprite = SPR_ITEMIMAGE;
	frame = (no - 1000);
      } else {
	// a weapon
	sprite = SPR_ARMSICONS;
	frame = no;
      }
      
      textbox.ItemImage.SetSprite(sprite, frame);
      textbox.ItemImage.SetVisible(true);
    } else
      { textbox.ItemImage.SetVisible(false); }
    break;
  }
  case 9:
    textbox.SetFace(stream->readShort());
    break;
  case 10:
    textbox.ResetState();
    game.mp.hand->ltbox_busy = true;
    break;
  }
  return false;
}

void sound(int snd);

bool handle_sound(NetStream *stream) {
  int id = stream->readInt();
  sound(id);
  return false;
}

bool handle_lock_keys(NetStream *stream) {
  bool lock = stream->readByte();
  player->inputs_locked = lock;
  return false;
}

bool handle_hp_update(NetStream *stream) {
  player->hp = stream->readByte();
  player->maxHealth = stream->readByte();
  return false;
}

bool handle_fade(NetStream *stream) {
  bool out = stream->readByte();
  char dir = stream->readByte();
  
  fade.Start(out ? FADE_OUT : FADE_IN, dir, SPR_FADE_DIAMOND);
  return false;
}

bool handle_damage(NetStream *stream) {
  int dmg = stream->readShort();
  
  player->DamageText->AddQty(dmg);
  return false;
}

bool handle_delta_xb(NetStream *stream) {
  int uid = stream->readShort();
  signed char dx = stream->readByte();
  LOOKUP(x+= dx << CSF);
  return false;
}
bool handle_delta_yb(NetStream *stream) {
  int uid = stream->readShort();
  signed char dy = stream->readByte();
  LOOKUP(y+= dy << CSF);
  return false;
}

bool handle_delta_xyb(NetStream *stream) {
  int uid = stream->readShort();
  signed char dx = stream->readByte();
  signed char dy = stream->readByte();
  LOOKUP(x+= dx << CSF);
  LOOKUP(y+= dy << CSF);
  return false;
}

bool handle_delta_xs(NetStream *stream) {
  int uid = stream->readShort();
  signed short dx = stream->readShort();
  LOOKUP(x+= dx << CSF);
  return false;
}
bool handle_delta_ys(NetStream *stream) {
  int uid = stream->readShort();
  signed short dy = stream->readShort();
  LOOKUP(y+= dy << CSF);
  return false;
}
bool handle_delta_xys(NetStream *stream) {
  int uid = stream->readShort();
  signed short dx = stream->readShort();
  signed short dy = stream->readShort();
  LOOKUP(x+= dx << CSF);
  LOOKUP(y+= dy << CSF);
  return false;
}

bool handle_pos_no_csf(NetStream *stream) {
  int uid = stream->readShort();
  int x = stream->readInt();
  int y = stream->readInt();
  LOOKUP(x = x << CSF);
  LOOKUP(y = y << CSF);
  return false;
}

bool handle_animate(NetStream *stream) {
  int uid = stream->readShort();
  int speed = stream->readByte();
  int first = stream->readShort();
  int last = stream->readShort();
  LOOKUP(Animate(speed, first, last));
  return false;
}

bool handle_init_wpn(NetStream *stream) {
  char wpn = stream->readByte();
  short ammo = stream->readShort();
  
  if(!player->weapons[wpn].hasWeapon) {
    player->weapons[wpn].ammo = 0;
    player->weapons[wpn].maxammo = ammo;
    player->weapons[wpn].level = 0;
    player->weapons[wpn].xp = 0;
    player->weapons[wpn].hasWeapon = true;
    player->curWeapon = wpn;
  } else 	// missile capacity powerups
  { player->weapons[wpn].maxammo += ammo; }
  return false;
}

bool handle_add_ammo(NetStream *stream) {
  char wpn = stream->readByte();
  short ammo = stream->readShort();
  
  AddAmmo(wpn, ammo);
  return false;
}

bool handle_set_sprite(NetStream *stream) {
  short uid = stream->readShort();
  short spr = stream->readShort();
  
  LOOKUP(sprite = spr);
  return false;
}

bool handle_update_block(NetStream *stream) {
  int x = stream->readShort();
  int y = stream->readShort();
  char t = stream->readByte();
  
  map.tiles[x][y] = t;
  return false;
}

bool handle_deal_damage(NetStream *stream) {
  short uid = stream->readShort();
  Object *o;
  LOOKUP_STORE(o);
  
  o->DamageWaiting += stream->readShort();
  
  if(o->shaketime < objprop[o->type].shaketime - 2)
  { o->shaketime = objprop[o->type].shaketime; }
  return false;
}

bool handle_delete_object(NetStream *stream) {
  short uid = stream->readShort();
  LOOKUP(Delete());
  return false;
}

bool handle_show_map_name(NetStream *stream) {
  map_show_map_name();
  return false;
}

bool handle_music(NetStream *stream) {
  unsigned short muzzic = stream->readShort();
  music(muzzic == 65535 ? music_lastsong() : muzzic);
  return false;
}

bool handle_focus(NetStream *stream) {
  short id2 = stream->readShort();
  short time = stream->readShort();
  Object *o = id2 ? FindObjectByID2(id2) : NULL;
  map_focus(o, time);
  return false;
}

bool handle_smoke_clouds(NetStream *stream) {
  int x = stream->readInt();
  int y = stream->readInt();
  int nclouds = stream->readInt();
  int rangex = stream->readInt();
  int rangey = stream->readInt();
  Object *o = NULL;
  short uid = stream->readShort();
  LOOKUP_STORE(o);
  SmokeXY(x, y, nclouds, rangex, rangey, o);
  return false;
}

bool handle_xp_update(NetStream *stream) {
  AddXP(stream->readShort());
}

PacketHandler handlers[] = {
  NULL,
  handle_handshake,
  handle_change_map,
  handle_spawn,
  handle_clear_objects,
  handle_new_object,
  handle_kill_object,
  handle_object_pos,
  handle_object_frame,
  handle_object_dir,
  handle_caret,
  handle_tsc,
  handle_tbox,
  handle_sound,
  handle_lock_keys,
  handle_hp_update,
  handle_fade,
  handle_object_type,
  handle_damage,
  handle_delta_xb,
  handle_delta_yb,
  handle_delta_xyb,
  handle_delta_xs,
  handle_delta_ys,
  handle_delta_xys,
  handle_pos_no_csf,
  handle_animate,
  handle_init_wpn,
  handle_add_ammo,
  handle_set_sprite,
  handle_update_block,
  handle_deal_damage,
  handle_delete_object,
  handle_show_map_name,
  handle_music,
  handle_focus,
  handle_smoke_clouds,
  handle_xp_update
};

extern bool tvis;
void disconnected(void *nethand) {
  game.reset();

  textbox.SetVisible(true);
  textbox.SetFlags(TB_LINE_AT_ONCE, true);
  textbox.SetText("Connection to server closed");
  textbox.ShowCursor(true);
  tvis = true;
}

void NetHandler::disconnect() {
  stream->closeSocket();
}

NetHandler::NetHandler(NetStream *str) {
  stream = str;
  stream->hooks.disconnect = &disconnected;
  stream->hooks.disconnect_data = this;
  v_nod = false;
  ltbox_busy = true;
}

void NetHandler::update_pos(int x, int y) {
  stream->writeByte(2);
  stream->writeInt(x);
  stream->writeInt(y);
}

void NetHandler::update_frame(int frame) {
  stream->writeByte(3);
  stream->writeInt(frame);
}

void NetHandler::update_dir(uint8_t dir) {
  stream->writeByte(4);
  stream->writeByte(dir);
}

void NetHandler::activate() {
  stream->writeByte(5);
}

void NetHandler::nod() {
  stream->writeByte(6);
}

void NetHandler::changeTBoxBusy(bool busy) {
  stream->writeByte(7);
  stream->writeByte(busy);
}

void NetHandler::textbox_ynj_result(bool result) {
  stream->writeByte(8);
  stream->writeByte(result);
}

void NetHandler::fire_pressed() {
  stream->writeByte(9);
}
void NetHandler::fire_released() {
  stream->writeByte(10);
}

void NetHandler::change_weapon(int id) {
  stream->writeByte(11);
  stream->writeByte(id);
}

void NetHandler::update_look(char look) {
  stream->writeByte(12);
  stream->writeByte(look);
}

void NetHandler::handlePackets() {
  //To be called from main thread
  int c = 0;
  
  while(stream->bytesAvailable() > 0) {
    if(c > 5)
    { break; }
    
    int id = stream->readByte();
    if(handlers[id](stream)) break;
    c++;
  }
  
  if(player->x != lx || player->y != ly) {
    update_pos(player->x, player->y);
    lx = player->x;
    ly = player->y;
  }
  
  if(player->frame != lf) {
    update_frame(player->frame);
    lf = player->frame;
  }
  
  if(player->dir != ldir) {
    update_dir(player->dir);
    ldir = player->dir;
  }

  if(player->look != llook) {
    update_look(player->look);
    llook = player->look;
  }
  
  if(textbox.IsBusy() != ltbox_busy) {
    changeTBoxBusy(textbox.IsBusy());
    ltbox_busy = textbox.IsBusy();
  }
  
  if(v_nod) {
    if((_inputs[JUMPKEY] && !v_nod_lastjump) ||
        (_inputs[FIREKEY] && !v_nod_lastfire)) {
      // hide the fact that the key was just pushed
      // so player doesn't jump/fire stupidly when dismissing textboxes
      lastinputs[JUMPKEY] |= _inputs[JUMPKEY];
      lastinputs[FIREKEY] |= _inputs[FIREKEY];
      lastpinputs[JUMPKEY] |= _inputs[JUMPKEY];
      lastpinputs[FIREKEY] |= _inputs[FIREKEY];
      nod();
      textbox.ShowCursor(false);
      v_nod = false;
    }
    
    v_nod_lastjump = _inputs[JUMPKEY];
    v_nod_lastfire = _inputs[FIREKEY];
  }
  
  if(textbox.YesNoPrompt.ResultReady()) {
    textbox_ynj_result(textbox.YesNoPrompt.GetResult() == YES);
    textbox.YesNoPrompt.ResetState();
  }
  
  if(!player->inputs_locked) {
    if(justpushed(PREVWPNKEY)) {
      stat_PrevWeapon();
      change_weapon(player->curWeapon);
    }
    
    if(justpushed(NEXTWPNKEY)) {
      stat_NextWeapon();
      change_weapon(player->curWeapon);
    }
    
    if(lastinputs[FIREKEY] && !_inputs[FIREKEY]) { fire_released(); }
    
    if(_inputs[FIREKEY] && !lastinputs[FIREKEY]) { fire_pressed(); }
  }

  stream->flush();
}

NetHandler::~NetHandler() {
  if(!stream->isClosed())   //Game exited
  { stream->closeSocket(); }
}
