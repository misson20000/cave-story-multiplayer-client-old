#ifndef CONCURRENTQUEUE_H
#define CONCURRENTQUEUE_H
#include<queue>
#include<mutex>
template<class T>
class ConcurrentQueue {
private:
  std::queue<T> *queue;
  std::mutex *mutex;
public:
  ConcurrentQueue() {
    mutex = new std::mutex();
    queue = new std::queue<T>();
  }
  int size() {
    mutex->lock();
    int s = queue->size();
    mutex->unlock();
    return s;
  }
  void push(T val) { //FIFO, not stack
    mutex->lock();
    queue->push(val);
    mutex->unlock();
  }
  T pop() { //FIFO, not stack
    mutex->lock();
    T val = queue->front();
    queue->pop();
    mutex->unlock();
    return val;
  }
};
#endif
