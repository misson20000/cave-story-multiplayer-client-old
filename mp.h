#ifndef MP_H
#define MP_H
#include"NetHandler.h"
#define MP_MAX_PLAYERS 256
class MP {
private:
  bool initted;
  bool init();
public:
  char addr[128];
  char port[6];
  bool connectSocket();
  NetHandler *hand;
};
#endif
